# Disable tests for now
%bcond_with check

%global packname magrittr
%global packver 2.0.3

Name:             R-%{packname}
Version:          %{packver}
Release:          1
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{packver}.tar.gz
License:          MIT
URL:              https://cran.r-project.org/web/packages/magrittr/index.html
Summary:          Provides a mechanism for chaining commands with a new forward-pipe operator
BuildRequires:    R-devel >= 3.0.0, tetex-latex, gcc
%if %{with check}
BuildRequires:    R-testthat
BuildRequires:    R-knitr
%endif

%description
Provides a mechanism for chaining commands with a new forward-pipe operator.
This operator will forward a value, or the result of an expression, into
the next function call/expression. There is flexible support for the type of
right-hand side expressions. For more information, see package vignette. To
quote Rene Magritte, "Ceci n'est pas un pipe."

%prep
%setup -q -c -n %{packname}

%build

%install
mkdir -p $RPM_BUILD_ROOT%{_libdir}/R/library
%{_bindir}/R CMD INSTALL -l $RPM_BUILD_ROOT%{_libdir}/R/library %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -rf $RPM_BUILD_ROOT%{_libdir}/R/library/R.css

%check
%if %{with check}
%{_bindir}/R CMD check %{packname}
%endif

%files
%dir %{_libdir}/R/library/%{packname}
%doc %{_libdir}/R/library/%{packname}/LICENSE
%doc %{_libdir}/R/library/%{packname}/html
%{_libdir}/R/library/%{packname}/DESCRIPTION
%{_libdir}/R/library/%{packname}/INDEX
%{_libdir}/R/library/%{packname}/NAMESPACE
%{_libdir}/R/library/%{packname}/NEWS.md
%{_libdir}/R/library/%{packname}/Meta
%{_libdir}/R/library/%{packname}/R
%{_libdir}/R/library/%{packname}/doc
%{_libdir}/R/library/%{packname}/help
%{_libdir}/R/library/%{packname}/libs/
%{_libdir}/R/library/%{packname}/logo*

%changelog
* Mon Oct 31 2022 hkgy <kaguyahatu@outlook.com> - 2.0.3-1
- Upgrade to v2.0.3

* Tue Jun 14 2022 misaka00251 <misaka00251@misakanet.cn> - 2.0.1-1
- Init package (Thanks to fedora team)
